# Hip Landing Pages CTA
Everything needed to manage landing pages for Hip client sites.

## Installation 
Install by running the following command from the root of your WordPress project:  
`composer require hipdevteam/landing-pages`
